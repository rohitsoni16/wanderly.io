import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:web_poc/model/DataModel.dart';

class APIs {
  static Future<dynamic> getData() async {
    var customHeaders = {
      "Accept": "application/json",
    };
    var resp = await http.get(
        Uri.parse("https://jsonplaceholder.typicode.com/posts/1/comments"),
        headers: customHeaders);
    print(resp);
    if (resp.statusCode == 200) {
      return jsonDecode(resp.body);
    } else {
      throw resp;
    }
  }
}

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:web_poc/controller/DataController.dart';
import 'package:web_poc/pages/HomePage.dart';

void main() {
  Get.put(DataController());
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'Flutter Demo',
        theme: ThemeData(
            primarySwatch: Colors.blue,
            primaryColor: Colors.black,
            accentColor: Colors.yellow),
        home: HomePage());
  }
}

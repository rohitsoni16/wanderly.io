import 'package:get/get.dart';
import 'package:web_poc/api/APIs.dart';
import 'package:web_poc/model/DataModel.dart';

class DataController extends GetxController {
  Rx<bool> _showProgress = false.obs;
  RxList<DataModel> _dataList = <DataModel>[].obs;

  bool get showProgress => _showProgress.value;
  // ignore: invalid_use_of_protected_member
  List<DataModel> get dataList => _dataList.value;

  getData() async {
    try {
      _showProgress.value = true;
      var resp = await APIs.getData();
      _dataList.value =
          resp.map<DataModel>((e) => DataModel.fromJson(e)).toList();
      print(dataList);
    } catch (e) {
      print(e);
    } finally {
      _showProgress.value = false;
    }
  }
}

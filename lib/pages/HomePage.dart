import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:web_poc/controller/DataController.dart';
import 'package:web_poc/model/DataModel.dart';
import 'package:web_poc/pages/CityDetailsPage.dart';
import 'package:web_poc/pages/WebSideBar.dart';
import 'package:web_poc/widgets/ProgressContainerView.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  DataController _dataController = Get.find<DataController>();

  bool isLargeScreen(BuildContext context) {
    return context.isTablet ||
        context.isLargeTablet ||
        context.isSmallTablet ||
        MediaQuery.of(context).size.width > 500;
  }

  @override
  void initState() {
    super.initState();
    _dataController.getData();
  }

  @override
  Widget build(context) => Scaffold(
        appBar: AppBar(title: Text("Wanderly")),
        drawer: MediaQuery.of(context).size.width < 500
            ? Drawer(
                child: WebSideBar(),
              )
            : null,
        body: Obx(
          () => ProgressContainerView(
            isProgressRunning: _dataController.showProgress,
            child: SafeArea(
              child: Center(
                child: MediaQuery.of(context).size.width < 500
                    ? _body()
                    : Row(
                        children: [
                          Container(width: 120.0, child: WebSideBar()),
                          Container(
                            width: MediaQuery.of(context).size.width - 180.0,
                            child: _body(),
                          )
                        ],
                      ),
              ),
            ),
          ),
        ),
      );

  Widget _body() {
    return Container(
      margin: EdgeInsets.all(16),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            padding: EdgeInsets.all(16),
            color: Colors.black,
            child: Row(
              children: [
                Text(
                  "Cities",
                  style: Theme.of(context)
                      .textTheme
                      .headline5!
                      .copyWith(color: Colors.white),
                ),
              ],
            ),
          ),
          SizedBox(height: 10),
          Text(
            "Choose a city and explore your interest.",
            style: Theme.of(context)
                .textTheme
                .subtitle1!
                .copyWith(color: Colors.black),
          ),
          SizedBox(height: 20),
          Expanded(
            child: GridView.count(
                crossAxisCount: isLargeScreen(context) ? 5 : 1,
                childAspectRatio: isLargeScreen(context) ? 1 : 1.5,
                children: _dataController.dataList
                    .map<Widget>((DataModel data) => data.getListItem(context))
                    .toList()),
          ),
        ],
      ),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:web_poc/pages/WebSideBar.dart';


class PlaceDetailsPage extends StatefulWidget {
  @override
  _PlaceDetailsPageState createState() => _PlaceDetailsPageState();
}

class _PlaceDetailsPageState extends State<PlaceDetailsPage> {
  @override
  Widget build(context) => Scaffold(
        appBar: AppBar(title: Text("Wanderly")),
        drawer: MediaQuery.of(context).size.width < 500
            ? Drawer(
                child: WebSideBar(),
              )
            : null,
        body: SafeArea(
          child: Center(
            child: MediaQuery.of(context).size.width < 500
                ? getItemsList()
                : Row(
                    children: [
                      Container(width: 120.0, child: WebSideBar()),
                      Container(
                        width:
                            (MediaQuery.of(context).size.width - 180.0) * 0.7,
                        child: getItemsList(),
                      ),
                      Container(
                          margin: EdgeInsets.all(16),
                          padding: EdgeInsets.all(16),
                          color: Colors.grey[200],
                          width:
                              (MediaQuery.of(context).size.width - 180.0) * 0.3,
                          child: Container())
                    ],
                  ),
          ),
        ),
      );
}

class getItemsList extends StatelessWidget {
  @override
  Widget build(context) {
    return Container(
      margin: EdgeInsets.all(16),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            padding: EdgeInsets.all(16),
            decoration: BoxDecoration(
                image: DecorationImage(
                    fit: BoxFit.cover,
                    image: NetworkImage(
                      "https://www.xchangecapital.com/hs-fs/hubfs/XCap/Sector-Experience-Banner.jpeg?width=5616&name=Sector-Experience-Banner.jpeg",
                    ))),
            child: Column(
              children: [
                Row(
                  children: [
                    Text(
                      "The Highlights",
                      style: Theme.of(context)
                          .textTheme
                          .headline4!
                          .copyWith(color: Colors.white),
                    ),
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    MaterialButton(
                      color: Colors.white,
                      textColor: Colors.black,
                      child: Text("Guides"),
                      onPressed: () {},
                    ),
                    SizedBox(width: 20),
                  ],
                ),
              ],
            ),
          ),
          SizedBox(height: 10),
          Text(
            "Last Updated 8/20",
            style: Get.textTheme.bodyText2!.copyWith(color: Colors.black),
          ),
          SizedBox(height: 20),
          Expanded(
            child: ListView.builder(
              itemBuilder: (ctx, i) {
                return Container(
                  margin: EdgeInsets.all(8),
                  padding: EdgeInsets.all(8),
                  child: Row(
                    children: [
                      Image.network(
                        "https://cdn.foodism.co.uk/featured_image/5c506d87c15c9.jpeg",
                        fit: BoxFit.cover,
                        height: 110,
                        width: 200,
                      ),
                      SizedBox(width: 20),
                      Expanded(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            Row(
                              children: [
                                Expanded(
                                  child: Text(
                                    "Ton's Pizzeria",
                                    style: Get.textTheme.headline6!
                                        .copyWith(color: Colors.black),
                                  ),
                                ),
                                IconButton(
                                    onPressed: () {},
                                    icon: Icon(Icons.favorite_border))
                              ],
                            ),
                            SizedBox(height: 12),
                            Text(
                              "Pizza is a type of food that was created in Italy (The Naples area). It is made with different toppings. Some of the most common toppings are cheese, sausages, pepperoni, vegetables, tomatoes, spices and herbs and basil.",
                              style: Get.textTheme.bodyText1!
                                  .copyWith(color: Colors.grey),
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.end,
                              children: [
                                InkWell(
                                  onTap: () {},
                                  child: Text(
                                    "See More >",
                                    style: Get.textTheme.subtitle1!
                                        .copyWith(color: Colors.black),
                                  ),
                                ),
                              ],
                            )
                          ],
                        ),
                      )
                    ],
                  ),
                );
              },
            ),
          ),
        ],
      ),
    );
  }
}

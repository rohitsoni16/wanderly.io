import 'dart:js';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:web_poc/controller/DataController.dart';
import 'package:web_poc/model/DataModel.dart';
import 'package:web_poc/pages/WebSideBar.dart';
import 'package:web_poc/widgets/ProgressContainerView.dart';

import 'PlaceDetaiilsPage.dart';

class CityDetailsPage extends StatefulWidget {
  @override
  _CityDetailsPageState createState() => _CityDetailsPageState();
}

class _CityDetailsPageState extends State<CityDetailsPage> {
  DataController _dataController = Get.find<DataController>();

  bool isLargeScreen(BuildContext context) {
    return context.isTablet ||
        context.isLargeTablet ||
        context.isSmallTablet ||
        MediaQuery.of(context).size.width > 500;
  }

  @override
  void initState() {
    super.initState();
    _dataController.getData();
  }

  @override
  Widget build(context) => Scaffold(
      appBar: AppBar(title: Text("Wanderly")),
      drawer: MediaQuery.of(context).size.width < 500
          ? Drawer(
              child: WebSideBar(),
            )
          : null,
      body: ProgressContainerView(
        isProgressRunning: _dataController.showProgress,
        child: Obx(
          () => SafeArea(
              child: Center(
                  child: MediaQuery.of(context).size.width < 500
                      ? _body(context)
                      : Row(children: [
                          Container(width: 120.0, child: WebSideBar()),
                          Container(
                              width: MediaQuery.of(context).size.width - 180.0,
                              child: _body(context))
                        ]))),
        ),
      ));

  Widget _body(BuildContext context) {
    return Container(
      margin: EdgeInsets.all(16),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            padding: EdgeInsets.all(16),
            color: Colors.black,
            child: Column(
              children: [
                Row(
                  children: [
                    Text(
                      "Austion Guides",
                      style: Theme.of(context)
                          .textTheme
                          .headline5!
                          .copyWith(color: Colors.white),
                    ),
                  ],
                ),
                Wrap(
                  children: [
                    MaterialButton(
                      color: Colors.white,
                      textColor: Colors.black,
                      child: Text("Letter From A Local"),
                      onPressed: () {},
                    ),
                    SizedBox(width: 20),
                    MaterialButton(
                      color: Colors.white,
                      textColor: Colors.black,
                      child: Text("Before You Go"),
                      onPressed: () {},
                    ),
                    SizedBox(width: 20),
                    MaterialButton(
                      color: Colors.white,
                      textColor: Colors.black,
                      child: Text("Your Favourite"),
                      onPressed: () {},
                    ),
                    SizedBox(width: 20),
                  ],
                ),
              ],
            ),
          ),
          SizedBox(height: 10),
          Text(
            "Here, Descriptions.....",
            style: Theme.of(context)
                .textTheme
                .subtitle1!
                .copyWith(color: Colors.black),
          ),
          SizedBox(height: 20),
          Expanded(
            child: GridView.count(
                crossAxisCount: isLargeScreen(context) ? 5 : 1,
                childAspectRatio: isLargeScreen(context) ? 1 : 1.5,
                children: _dataController.dataList.map((DataModel data) {
                  return InkWell(
                    onTap: () {
                      Get.to(PlaceDetailsPage());
                    },
                    child: Container(
                      height: 140,
                      width: 200,
                      margin: EdgeInsets.all(8),
                      decoration: BoxDecoration(
                          image: DecorationImage(
                              fit: BoxFit.fill,
                              image: NetworkImage(data.image))),
                      child: Container(
                        decoration: BoxDecoration(
                          gradient: LinearGradient(
                              colors: [Colors.transparent, Colors.black54],
                              begin: Alignment.topCenter,
                              end: Alignment.bottomCenter),
                        ),
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.end,
                            mainAxisSize: MainAxisSize.min,
                            children: [
                              Text(
                                "The Highlights",
                                style: Theme.of(context)
                                    .textTheme
                                    .subtitle1!
                                    .copyWith(
                                        color: Colors.white,
                                        fontWeight: FontWeight.bold),
                              ),
                              Text(
                                "Natural Place",
                                style: Theme.of(context)
                                    .textTheme
                                    .caption!
                                    .copyWith(color: Colors.white),
                              )
                            ],
                          ),
                        ),
                      ),
                    ),
                  );
                }).toList()),
          ),
        ],
      ),
    );
  }
}
